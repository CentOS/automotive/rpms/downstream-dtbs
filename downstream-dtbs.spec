%global             dtbs_version 1
%global             latest_kernel 5.14.0-570%{dist}.%{_arch}
%global             latest_kernel_automotive 5.14.0-570.519%{dist}.%{_arch}

%global             srcrev 2e052b6ca71aacac6422a0044f23b5325ccc35a0

%if "%{dist}" == ".el9iv"
%{!?kernel_version:%{expand:%%global kernel_version %{latest_kernel_automotive}}}
%else
%{!?kernel_version:%{expand:%%global kernel_version %{latest_kernel}}}
%endif

%define             debug_package %{nil}

Name:               downstream-dtbs
%if "%{dist}" == ".el9iv"
Version:            %{lua:print((string.gsub(string.match(rpm.expand("%{kernel_version}"), "(%d+%.%d+%.%d+%-%d+%.%d+%.?[^.]*)%.el9iv"),"-","~")))}.%{dtbs_version}
%else
Version:            %{lua:print((string.gsub(string.match(rpm.expand("%{kernel_version}"), "(%d+%.%d+%.%d+%-%d+%.?[^.]*)"..rpm.expand("%{dist}")),"-","~")))}.%{dtbs_version}
%endif
Release:            1%{?dist}
Summary:            Downstream device tree blobs

License:            GPLv2
URL:                https://gitlab.com/CentOS/automotive/src/downstream-dtbs.git
Source:             https://gitlab.com/CentOS/automotive/src/downstream-dtbs/-/archive/%{srcrev}/downstream-dtbs-%{srcrev}.tar.gz

ExclusiveArch:      aarch64
%if "%{dist}" == ".el9iv"
BuildRequires:      kernel-automotive-devel-uname-r = %{kernel_version}
%else
BuildRequires:      kernel-devel-uname-r = %{kernel_version}
%endif
BuildRequires:      kernel-rpm-macros


%description
%{summary}


%prep
%setup -T -b 0 -q -n downstream-dtbs-%{srcrev}


# Use kmodtool from kernel-rpm-macros for finding the correct path.
%global kmodtool    /usr/lib/rpm/redhat/kmodtool
%global kernel_src  %(%{kmodtool} kernel_source %{kernel_version})


%build
%make_build KERNEL_VERSION=%{kernel_version} KERNEL_SRC=%{kernel_src} dtbs


%install
%make_build KERNEL_VERSION=%{kernel_version} KERNEL_SRC=%{kernel_src} INSTALL_DTBS_PATH=%{buildroot}/lib/modules/%{kernel_version}/dtb/extra dtbs_install


%post
cp -r /lib/modules/%{kernel_version}/dtb/extra /boot/dtb-%{kernel_version}/extra


%postun
rm -rf /boot/dtb-%{kernel_version}/extra


%files
/lib/modules/%{kernel_version}/dtb/extra


%changelog
* Tue Feb 11 2025 Eric Chanudet <echanude@redhat.com> 5.14.0-563.511.1-1
- Bump src version and target kernel version.
- Set ExclusiveArch until device-tree on a different arch make it here.

* Thu Jan 30 2025 Eric Chanudet <echanude@redhat.com> 5.14.0-549.498.1-1
- Refactor specfile around a Makefile

* Wed Apr 3 2024 Lucas Karpinski <lkarpins@redhat.com> 0.1
- downstream-dtbs: initial packaging
